# Loops
# Написать цикл, который опрашивает пользователя и
# выводит рандомное число.
# Цикл должен прерываться по символу Q(q)

from random import randint


def survey_cycle():
    while True:
        request = input("Enter value: ")
        if request.lower() == 'q':
            print('Quit')
            break
        print(randint(1, 1000))


if __name__ == '__main__':
    survey_cycle()
