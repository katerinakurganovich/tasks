# Найти сумму всех чисел, меньших 1000, кратных 3 и 7
# Реализовать через filter/map/reduce

from functools import reduce
from typing import List


def sum_of_numb_mult_3_or_7_by_filter():
    original_list: List[int] = list(range(1, 1000))
    filter_list: List[int] = list(filter(lambda num: num % 3 == 0 and num % 7 == 0, original_list))
    return sum(filter_list)


def sum_of_numb_mult_3_or_7_by_map():
    original_list: List[int] = list(range(1, 1000))
    filter_list: List[int] = list(map(lambda num: num if num % 3 == 0 and num % 7 == 0 else 0, original_list))
    return sum(filter_list)


def sum_of_numb_mult_3_or_7_by_reduce():
    original_list: List[int] = list(range(1, 1000))
    filter_list: List[int] = [num for num in original_list if num % 3 == 0 and num % 7 == 0]
    sum_numbers: int = reduce(lambda x_1, x_2: x_1 + x_2, filter_list)
    return sum_numbers


def test_values():
    assert sum_of_numb_mult_3_or_7_by_filter() == sum_of_numb_mult_3_or_7_by_map()\
           == sum_of_numb_mult_3_or_7_by_reduce()
