# Реализовать декоратор кэширования вызова функции
# В случае, если вызывается функция c одинаковыми
# параметрами, то результат не должен заново вычисляться,
# а возвращаться из хранилища

from functools import wraps
import time


def cache_decorator(func):
    cache: dict = {}

    @wraps(func)
    def wrapper(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrapper


@cache_decorator
def fib(num):
    if num < 2:
        return num
    return fib(num - 1) + fib(num - 2)


def test_lead_time():
    def fib_without_cache(num):
        if num < 2:
            return num
        return fib(num - 1) + fib(num - 2)

    start_time = time.perf_counter()
    fib_without_cache(50)
    lead_time = time.perf_counter() - start_time
    fib(40)
    start_time_with_cache = time.perf_counter()
    fib(50)
    lead_time_with_cache = time.perf_counter() - start_time_with_cache
    assert lead_time_with_cache < lead_time
