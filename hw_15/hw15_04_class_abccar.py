# Реализовать интерфейс класса Car.

import abc


class ABCCar(abc.ABC):
    @abc.abstractmethod
    def open_door(self):
        pass

    @abc.abstractmethod
    def close_door(self):
        pass

    @abc.abstractmethod
    def braking(self):
        pass

    @abc.abstractmethod
    def reverse(self):
        pass
