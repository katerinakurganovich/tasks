# Реализовать класс Car в соответствии с интерфейсом.
# В нем реализовать метод speed, возвращающий текущую скорость.
# Реализовать декоратор метода - в случае превышения скорости - декоратор
# должен логировать в logger.error сообщение о превышении лимита

import logging
from hw_15.hw15_04_class_abccar import ABCCar


def limit_decorator(func):
    def wrapper(self):
        speed_limit = 50
        if func(self) > speed_limit:
            logging.error('Attention! Limit is exceeded')
        return func(self)
    return wrapper


class BaseCar(ABCCar):
    """A class to describe a cars."""
    def __init__(self, color, brand, current_speed, door_status='close'):
        """Constructs all the necessary parameters for the car object.

        :param color: Color of car
        :param brand: brand of car
        :param current_speed: Current speed of car
        :param door_status: Status of door. Open/Close
        """
        self._color = color
        self._brand = brand
        self._current_speed = current_speed
        self._door_status = door_status

    def braking(self):
        """Change speed to 0."""
        self._current_speed = 0
        return self._current_speed

    def open_door(self):
        """ Change door status to 'Open'."""
        self._door_status = 'open'
        return self._door_status

    def close_door(self):
        """Change door status to 'Close'."""
        self._door_status = 'close'
        return self._door_status

    def reverse(self):
        """To reverse the car"""
        self._current_speed = - self._current_speed
        return self._current_speed

    @property
    @limit_decorator
    def speed(self):
        """ Return current speed of car"""
        return self._current_speed


def test_braking():
    ford = BaseCar('black', 'Ford', 60)
    assert ford.braking() == 0


def test_open_close_door():
    ford = BaseCar('black', 'Ford', 60)
    assert ford.open_door() == 'open'
    assert ford.close_door() == 'close'


def test_reverse():
    ford = BaseCar('black', 'Ford', 60)
    assert ford.reverse() == -60
