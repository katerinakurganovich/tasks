# Реализовать класс-миксин, добавляющий классу Car атрибут
# spoiler.
# Spoiler должен влиять на Car.speed , увеличивая ее на значение N.

from hw_15.hw15_05_class_car import BaseCar


class SpoilerMixin:
    def spoiler(self, value):
        self._current_speed = self._current_speed + value
        return self._current_speed


class Car(BaseCar, SpoilerMixin):
    pass


def test_for_spoiler():
    ford = Car('black', 'Ford', 40)
    ford.spoiler(5)
    assert ford.speed == 45
