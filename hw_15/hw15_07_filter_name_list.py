# Функция принимает список имен.
# Функция возвращает список отфильтрованных имен по переданному литералу

from typing import List


def filter_name_starting_with_letter(name_list: List[str], letter: str):
    filter_list_of_names: List[str] = list(filter(lambda x: x[0].upper() == letter.upper(), name_list))
    title_name_list: List[str] = list(map(lambda x: x.title(), filter_list_of_names))
    title_name_list.sort()
    return title_name_list


def test_filter():
    names: List[str] = ['adam', 'Bob', 'Adrian']
    assert filter_name_starting_with_letter(names, 'a') == ['Adam', 'Adrian']
