# реализовать общий для всех классов
# counter и метод, который его изменяет на 1
# изменения должны быть видны всем дочерним классам


class Animal:
    _counter: int = 0

    @staticmethod
    def counter():
        return Animal._counter

    @staticmethod
    def change_counter():
        Animal._counter += 1


class Pet(Animal):
    pass


class Cat(Pet):
    pass


def test_counter():
    animal = Animal()
    pet = Pet()
    cat = Cat()
    animal.change_counter()
    pet.change_counter()
    cat.change_counter()
    assert (Animal.counter(), Pet.counter(), cat.counter()) == (3, 3, 3)
