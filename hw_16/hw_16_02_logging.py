# реализовать класс, который имеет атрибут журнала логирования,
# куда попадают строки вида
# <дата-время вызова>-<класс>-<вызываемый метод>
# реализовать декоратор, которым можно будет оборачивать тот метод,
# который мы захотим залогировать

from datetime import datetime
from functools import wraps
from typing import List


class DataBook:
    LOGGING_BOOK: List[str] = []


def logging(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        current_date = datetime.now()
        class_meth = self.__class__.__name__
        method = func.__name__
        func(self, *args, **kwargs)
        DataBook.LOGGING_BOOK.append(f"{current_date} - {class_meth} - {method}")
        return func(self, *args, **kwargs)
    return wrapper


class Cat:
    def __init__(self):
        self.speed = 0

    @logging
    def run(self):
        self.speed += 10

    @logging
    def stop(self):
        self.speed = 0


def test_logging():
    cat = Cat()
    cat.run()
    cat.stop()
    assert len(DataBook.LOGGING_BOOK) == 2
