# Реализовать мета-класс фабрику, которая
# в зависимости от атрибута shape_type = ['Rectangle', 'Triangle', 'Circle']
# возвращает нужный класс с свойством square

from math import pi


def rectangle_square(self):
    return self.length * self.width


def triangle_square(self):
    semi_p = (self.first_side + self.second_side + self.third_side) / 2
    square = (semi_p * (semi_p - self.first_side) * (semi_p - self.second_side) * (semi_p - self.third_side)) ** 0.5
    return square


def circle_square(self):
    return pi * self.radius**2


class ShapeMeta(type):
    def __new__(mcs, shape_type, parents, attrs):
        if shape_type == 'Rectangle':
            attrs['square'] = rectangle_square
        elif shape_type == 'Triangle':
            attrs['square'] = triangle_square
        else:
            attrs['square'] = circle_square
        return super().__new__(mcs, shape_type, parents, attrs)


def test_shape_meta():
    rectangle_class = ShapeMeta('Rectangle', (), {})
    assert rectangle_class.__dict__['square'] == rectangle_square
    rectangle = rectangle_class()
    assert 'square' in rectangle.__dir__()
