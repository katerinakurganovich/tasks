# Класс-контейнер хранит экзепмляры класса Shape.
# Реализациями Shape могут быть 'Rectangle', 'Triangle', 'Circle'
# Shape должен предоставлять интерфейс square
# Предусмотреть проверки
# Контейнер должен иметь свойство square - возвращающее общую
#  площадь всех фигур

import abc
from math import pi
from typing import Union


class ABCShape(abc.ABC):
    @abc.abstractmethod
    def square(self):
        pass


class SomeFigure(ABCShape):
    """A class to describe a SumFigure."""
    def __init__(self,  value):
        """Constructs all the necessary parameters for the SomeFigure object"""
        self.value = value

    def square(self):
        """Calculates the square of the figure"""
        return self.value

    def __add__(self, other):
        """ Return self + value. """
        return SomeFigure(self.value + other.value)

    def __repr__(self):
        """Represents a class's objects as a string."""
        return f'SomeFigure ({round(self.value, 2)})'


class Rectangle(ABCShape):
    """A class to describe a rectangles."""
    def __init__(self, width: Union[int, float], length: Union[int, float]):
        """Constructs all the necessary parameters for the rectangle object.

        :param width: One side of the rectangle.
        :param length: Other side of the rectangle.
        """
        self.width = width
        self.length = length

    def square(self):
        """Calculates the square of the rectangle"""
        return self.width * self.length

    def __add__(self, other):
        """ Return self + value. """
        return SomeFigure(self.square() + other.square())

    def __mul__(self, other):
        """ Return self * value. """
        return SomeFigure(self.square() * other.square())

    def __repr__(self):
        """Represents a class's objects as a string."""
        return f'Rectangle {self.width, self.length}'


class Triangle(ABCShape):
    """A class to describe a triangles."""
    def __init__(self, first_side: Union[int, float],
                 second_side: Union[int, float],
                 third_side: Union[int, float]):
        """Constructs all the necessary parameters for the triangle object.

        :param first_side: The first side of triangle.
        :param second_side: The second side of triangle.
        :param third_side: The third side of triangle.
        """
        self.semi_per = (first_side + second_side + third_side)/2
        self.first_side = first_side
        self.second_side = second_side
        self.third_side = third_side

    def square(self):
        """Calculates the square of the triangle"""
        square = (self.semi_per * (self.semi_per - self.first_side)*(self.semi_per - self.second_side) *
                  (self.semi_per - self.third_side))**0.5
        return square

    def __add__(self, other):
        """ Return self + value. """
        return SomeFigure(self.square() + other.square())

    def __mul__(self, other):
        """ Return self * value. """
        return SomeFigure(self.square() * other.square())

    def __repr__(self):
        """Represents a class's objects as a string."""
        return f'Triangle {self.first_side, self.second_side, self.third_side}'


class Circle(ABCShape):
    """A class to describe a circles."""
    def __init__(self, radius: Union[int, float]):
        """Constructs all the necessary parameters for the circle object.

        :param radius: The radius of the circle.
        """
        self.radius = radius

    def square(self):
        """Calculates the square of the circle"""
        return pi * self.radius ** 2

    def __add__(self, other):
        """ Return self + value. """
        return SomeFigure(self.square() + other.square())

    def __mul__(self, other):
        """ Return self * value. """
        return SomeFigure(self.square() * other.square())

    def __repr__(self):
        """Represents a class's objects as a string."""
        return f'Circle ({self.radius})'


class Container:
    """A class to describe a containers."""
    def __init__(self):
        """Constructs all the necessary parameters for the container object."""
        self.shapes: list = []
        self.square: float = 0.0

    def add_rectangle(self, width: Union[int, float], length: Union[int, float]):
        """ Adds the rectangle objects to container"""
        rectangle = Rectangle(width, length)
        self.shapes.append(rectangle)
        self.square += rectangle.square()

    def add_triangle(self, first_side: Union[int, float],
                     second_side: Union[int, float],
                     third_side: Union[int, float]):
        """ Adds the triangle objects to container"""
        triangle = Triangle(first_side, second_side, third_side)
        self.shapes.append(triangle)
        self.square += triangle.square()

    def add_circle(self, radius: Union[int, float]):
        """ Adds the circle objects to container"""
        circle = Circle(radius)
        self.shapes.append(circle)
        self.square += circle.square()


def test_container():
    container = Container()
    container.add_rectangle(3, 4)
    assert container.square == 12
    container.add_circle(5)
    assert container.square == 12 + pi * 5**2
    container.add_triangle(3, 4, 5)
    assert container.square == 12 + pi * 5**2 + 0.5 * 3 * 4
    assert len(container.shapes) == 3
