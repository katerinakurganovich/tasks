# Реализовать дата-класс HostSettings.
# Должен иметь свойства ip_addr, port, hostname, username, password, auth_type

from dataclasses import dataclass


@dataclass
class HostSettings:
    ip_addr: str
    port: int
    hostname: str
    username: str
    password: str
    auth_type: str


def test_dataclass():
    host = HostSettings("192.168.1.1", 5000, "Host_name", "Katsiaryna", "password", "Windows10")
    assert (host.ip_addr, host.port, host.hostname, host.username,
            host.password, host.auth_type) == ("192.168.1.1", 5000, "Host_name", "Katsiaryna", "password", "Windows10")
