# Из прошлых заданий доработать класс Car:
# Авто должен иметь топливный бак,
# рассчитанный на количество литров N
# При создании объекта машины бак полон и рассчитан на определенное количество км.
# При различных диапазонах скорости реализовать различный расход топлива:
# 0-40 км/ч == 10 литров/100 км
# 40-60 км/ч= 8 литров/100 км
# 60-90 км/ч = 6 литров/100 км
# 90-120 = 5.5 литров/100 км
# 120-140 = 8 литров/100 км
# 140-180 = 11 литров/100 км
# Реализовать ограничение скорости путем возбуждения исключения SpeedLimitError
# Реализовать исключение FuelFinishError (при остатке хода 0км)
# Реализовать исключение StopDrive (окончание пути)
# Считать один проход цикла равным времени 1 час.
# Реализовать:
# - ввод расстояния L, которое необходимо проехать.
# Реализовать обработку ошибок невалидных данных.
# - на каждую итерацию цикла происходит увеличение скорости на 5 км/ч.
# Авто начинает двигаться со скорости 30 км/ч
# - рандомно возникают знаки ограничения скорости
#  (диапазон пределов: 40, 60, 90, 120 км/ч)
# - при появления знака должно возбуждаться исключение и авто должен сбрасывать
#  скорость до
# нужного предела (+ перерасчет скорости и расхода топлива)
# - при исзрасходовании топлива должно возбуждаться исключение
#  FuelFinishError, после чего бак снова заполняется
# и авто начинает движение со скорости 30 км
# - при прохождении всего расстояния
#  должно возбуждаться StopDrive - конец программы
# - логировать маршрут следования каждые 10 км.
# * поддержать использование
# авто со спойлером (учесть расход +n литров и +S скорость км/ч)

import logging
from typing import Union, List
from hw_15.hw15_05_class_car import BaseCar
from hw_15.hw15_06_mixin_add_spoiler import SpoilerMixin
from random import choice


class FuelFinishError(Exception):
    def __str__(self):
        return 'FuelFinishError'


class SpeedLimitError(Exception):
    def __str__(self):
        return 'SpeedLimitError'


class StopDrive(Exception):
    def __str__(self):
        return 'StopDrive'


class WrongInstance(Exception):
    def __str__(self):
        return 'WrongInstance'


class Car(BaseCar, SpoilerMixin):
    def __init__(self, fuel_tank,  color, brand, current_speed, door_status='close'):
        super().__init__(color, brand, current_speed, door_status)
        self._fuel_tank = fuel_tank

    @property
    def get_speed(self):
        return self._current_speed

    @property
    def get_fuel_tank(self):
        return self._fuel_tank

    def change_speed(self, value):
        self._current_speed = value
        return self._current_speed


def init_logging():
    """Setting up the logging module."""
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    logger_handler = logging.FileHandler('python_logging.log')
    logger_handler.setLevel(logging.INFO)
    logger_formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    logger_handler.setFormatter(logger_formatter)
    logger.addHandler(logger_handler)
    logger.info('Start!')
    return logger


def data_logging(logger, current_way, way, value):
    """Route logging."""
    for i in range(0, way + 10, 10):
        if current_way - value < i <= current_way:
            logger.info(f'The remaining distance  - {way - i}')


def fuel_consumption(speed: Union[int, float]) -> int:
    """Calculates fuel consumption."""
    liters: Union[int, float] = 0
    if 0 < speed <= 40:
        liters = 10
    elif 40 < speed <= 60:
        liters = 8
    elif 60 < speed <= 90:
        liters = 6
    elif 90 < speed <= 120:
        liters = 5.5
    elif 120 < speed <= 140:
        liters = 8
    elif 140 < speed <= 180:
        liters = 11
    return liters


def random_sign() -> int:
    """Returns a random sign."""
    limit_speed: List[int] = [40, 60, 90, 120]
    return choice(limit_speed)


def if_speed_more_speed_sign(logger, car_object, random_speed_sign):
    """Actions in case speeding."""
    try:
        raise SpeedLimitError
    except SpeedLimitError as er:
        logger.info(f'{er} - Warning! Speeding!')
        speed = car_object.change_speed(random_speed_sign)


def is_not_fuel_enough(logger, car_object):
    """Actions in case of lack of fuel."""
    try:
        raise FuelFinishError
    except FuelFinishError as er:
        logger.info(f'{er} - Warning! Out of fuel!')
        car_object.change_speed(30)


def is_current_way_more_then_way(logger):
    """Actions in case of arrival """
    try:
        raise StopDrive
    except StopDrive as er:
        logger.info(f'{er} - Arrived!')


def class_instance(car_object, class_name):
    """Checking for class membership"""
    if not isinstance(car_object, class_name):
        try:
            raise WrongInstance
        except WrongInstance as er:
            print(f'{er} - Enter right object!')
            return False
    else:
        return True


def drive_car(car_object: Car, way: int):
    current_way: Union[int, float] = 0
    current_fuel: Union[int, float] = 0
    logger = init_logging()
    while True:
        if not class_instance(car_object, Car):
            break
        if not class_instance(way, int):
            break
        speed: Union[int, float] = car_object.get_speed
        random_speed_sign: int = random_sign()
        if speed > random_speed_sign:
            if_speed_more_speed_sign(logger, car_object, random_speed_sign)
        speed = car_object.spoiler(5)
        fuel = fuel_consumption(speed)
        possible_way = (car_object.get_fuel_tank - current_fuel) * 100 / fuel
        if possible_way < speed * 1:
            current_way += possible_way
            current_fuel += fuel * possible_way / 100
            is_not_fuel_enough(logger, car_object)
            current_fuel = 0
            data_logging(logger, current_way, way, possible_way)
            continue
        else:
            current_way += speed * 1
            current_fuel += fuel * speed * 1 / 100
            data_logging(logger, current_way, way, speed * 1)
        if current_way >= way:
            is_current_way_more_then_way(logger)
            break


if __name__ == "__main__":
    drive_car(Car(150, 'red', 'BMW', 30), 500)
