from sqlql import *
from session import Cursor
from typing import Any

tables: dict = {'Departments': table_departments,
                'Employees': table_employees,
                'Jobs': table_jobs,
                'Location': table_location,
                'Countries': table_countries}

# Create tables


def create_table(table_name: str) -> None:
    with Cursor() as cur:
        try:
            cur.execute(tables[table_name])
        except KeyError:
            print('Incorrect table name')
        else:
            print(f'Table {table_name} is created')

# Add data into tables


def add_department(name: str, manager_id: int, location_id: int) -> None:
    with Cursor() as cur:
        cur.execute(department, (name, manager_id, location_id))
        cur.execute("COMMIT")


def add_employee(first_name: str, last_name: str, email: str, phone_number: str,
                 hire_date: str, job_id: int, manager_id: int, department_id: int) -> None:
    with Cursor() as cur:
        cur.execute(employee, (first_name, last_name, email, phone_number,
                               hire_date, job_id, manager_id, department_id))
        cur.execute("COMMIT")


def add_job(title: str, min_salary: int, max_salary: int) -> None:
    with Cursor() as cur:
        cur.execute(job, (title, min_salary, max_salary))
        cur.execute("COMMIT")


def add_location(street_address: str, postal_code: int, city: str, country_id: int):
    with Cursor() as cur:
        cur.execute(location, (street_address, postal_code, city, country_id))
        cur.execute("COMMIT")


def add_country(name: str):
    with Cursor() as cur:
        cur.execute(country, (name,))
        cur.execute("COMMIT")

# Delete data from tables


def delete_data(table_name: str, attr_name: str, value: Any) -> None:
    with Cursor() as cur:
        cur.execute(f"""
        DELETE FROM {table_name}
        WHERE {attr_name} = ?
        """, (value,))
        cur.execute("COMMIT")

# Update data in tables


def update_data(table_name: str, attr_name: str, new_value: Any, old_value: Any) -> None:
    with Cursor() as cur:
        cur.execute(f"""
        UPDATE {table_name}
        SET {attr_name} = ?
        WHERE {attr_name} = ?
        """, (new_value, old_value))
        cur.execute("COMMIT")

# Select data from tables


def employers_count_of_department(department_name: str) -> list:
    with Cursor() as cur:
        cur.execute(count_of_employees, (department_name,))
        res = cur.fetchall()
    return res


def count_of_managers() -> list:
    with Cursor() as cur:
        cur.execute(managers)
        res = cur.fetchall()
    return res


def avg_salary() -> list:
    with Cursor() as cur:
        cur.execute(salary)
        res = cur.fetchall()
    return res


def unique_cities() -> list:
    with Cursor() as cur:
        cur.execute(cities)
        res = cur.fetchall()
    return res
