import sqlite3


class Cursor:
    def __init__(self):
        self.connect = None
        self.cursor = None

    def __enter__(self):
        self.connect = sqlite3.connect("employees.sqlite")
        self.cursor = self.connect.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cursor.close()
        self.connect.close()
