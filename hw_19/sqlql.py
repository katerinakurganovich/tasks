table_departments = """
CREATE TABLE IF NOT EXISTS Departments (
department_id INTEGER PRIMARY KEY,
name TEXT NOT NULL, 
manager_id INTEGER NOT NULL,
location_id INTEGER NOT NULL 
);
"""

table_employees = """
CREATE TABLE IF NOT EXISTS Employees (
employee_id INTEGER PRIMARY KEY,
first_name TEXT NOT NULL, 
last_name TEXT NOT NULL,
email TEXT UNIQUE,
phone_number TEXT UNIQUE,
hire_date DATE NOT NULL,
job_id INTEGER NOT NULL,
manager_id INTEGER NOT NULL,
department_id INTEGER NOT NULL,
FOREIGN KEY (manager_id) REFERENCES Departments (manager_id),
FOREIGN KEY (department_id) REFERENCES Departments (department_id)
);
"""

table_jobs = """
CREATE TABLE IF NOT EXISTS Jobs (
job_id INTEGER PRIMARY KEY,
title TEXT NOT NULL, 
min_salary INTEGER,
max_salary INTEGER,
FOREIGN KEY (job_id) REFERENCES Employees (job_id)
);
"""

table_location = """
CREATE TABLE IF NOT EXISTS Location (
location_id INTEGER PRIMARY KEY, 
street_address TEXT, 
postal_code TEXT,
city TEXT,
country_id INTEGER NOT NULL,
FOREIGN KEY (location_id) REFERENCES Departments (location_id) 
);
"""

table_countries = """
CREATE TABLE IF NOT EXISTS Countries (
country_id INTEGER PRIMARY KEY, 
name TEXT,
FOREIGN KEY (country_id) REFERENCES Location (country_id)
);
"""

department = """
INSERT INTO Departments (name, manager_id, location_id)
VALUES
(?, ?, ?);
"""

employee = """
INSERT INTO Employees (first_name, last_name, 
                        email, phone_number, hire_date,job_id,
                        manager_id, department_id)
VALUES
(?, ?, ?, ?, ?, ?, ?, ?);
"""

job = """
INSERT INTO Jobs (title, min_salary, max_salary)
VALUES
(?, ?, ?);
"""

location = """
INSERT INTO Location (street_address, postal_code, city, country_id)
VALUES
(?, ?, ?, ?);
"""

country = """
INSERT INTO Countries (name)
VALUES
(?);
"""

count_of_employees = """
SELECT COUNT(*) AS count FROM Employees
JOIN Departments AS D on D.department_id = Employees.department_id
WHERE D.name = ?
"""

managers = """
SELECT COUNT(DISTINCT manager_id) AS count FROM Departments
"""

salary = """
SELECT AVG((max_salary + min_salary)/2) AS avg_salary FROM Jobs
"""

cities = """
SELECT DISTINCT city FROM Location
ORDER BY city
"""