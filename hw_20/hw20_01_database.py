# Ex.1
# Найти заказчиков(customer) и обслуживающих их заказы сотрудкников(employees)
# Заказчики и сотрудники из города London, доставка осуществляется компанией Speedy Express.
# Вывести компанию заказчика и ФИО сотрудника

from hw20_session import DBSession
from models import *
from hw20_sqlql import select_customers_and_employees


def get_customers_and_employees_raw_sql():
    with DBSession() as session:
        results = session.execute(select_customers_and_employees)
        for result in results:
            print(result)


def get_customers_and_employees_orm():
    with DBSession() as session:
        results = session.query(Orders).join(Employees).join(
            Shippers).join(Customers).filter(Customers.city == "London",
                                             Employees.city == "London",
                                             Shippers.company_name == "Speedy Express"
                                             ).with_entities(Customers.company_name,
                                                             Employees.last_name,
                                                             Employees.first_name)

        for result in results:
            print(result)
