# Ex.2
# Найти заказчиков, у которых нет ни одного заказа. Вывести имя заказчика и order_id.

from hw20_session import DBSession
from models import *
from hw20_sqlql import select_customers_who_did_not_order


def get_customers_who_did_not_order_raw_sql():
    with DBSession() as session:
        results = session.execute(select_customers_who_did_not_order)
        for result in results:
            print(result)


def get_customers_who_did_not_order_orm():
    with DBSession() as session:
        subquery = session.query(Orders.customer_id).subquery()
        results = session.query(Customers).filter(
            Customers.customer_id.notin_(subquery)
        ).with_entities(Customers.customer_id, Customers.company_name)
        for result in results:
            print(result)
