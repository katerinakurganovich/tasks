# Ex.3
# Найти активные (поле discontinued) продукты из категории Condiments и Meat/Poultry, которых в продаже менее 100 единиц
# Вывести наименование продуктов, кол-во единиц в продаже, имя контакта поставщика и его телефонный номер.

from hw20_session import DBSession
from models import *
from hw20_sqlql import select_products


def get_products_raw_sql():
    with DBSession() as session:
        results = session.execute(select_products)
        for result in results:
            print(result)


def get_products_orm():
    with DBSession() as session:
        results = session.query(Products).join(Categories).join(Suppliers).filter(
            Products.discontinued == 1, Categories.category_name.in_(('Condiments', 'Meat/Poultry')),
            Products.units_in_stock < 100
        ).with_entities(Products.product_name, Products.units_in_stock, Suppliers.contact_name, Suppliers.phone)
        for result in results:
            print(result)
