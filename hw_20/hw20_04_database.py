# Ex.4
# Подсчитать количество товаров в каждой категории, вывести по убыванию.

from hw20_session import DBSession
from models import *
from hw20_sqlql import select_units_of_products
from sqlalchemy.sql import func


def get_units_of_products_raw_sql():
    with DBSession() as session:
        results = session.execute(select_units_of_products)
        for result in results:
            print(result)


def get_units_of_products_orm():
    with DBSession() as session:
        units = func.sum(Products.units_in_stock)
        query = session.query(Categories.category_name, units).join(Categories)
        query_group_by_category = query.group_by(Categories.category_name)
        results = query_group_by_category.order_by(units.desc()).all()
        for result in results:
            print(result)
