import sqlalchemy as db
from hw_20.settings import DB_CONNECTION
from sqlalchemy.orm import sessionmaker


class DBSession:
    engine = db.create_engine(DB_CONNECTION)
    Session = sessionmaker(bind=engine)

    def __init__(self, with_commit=False):
        self.session = None
        self.commit = with_commit

    def __enter__(self):
        self.session = self.__class__.Session()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.session.rollback()
        else:
            if self.commit:
                self.session.commit()
        self.session.close()
