select_customers_and_employees = """
SELECT customers.company_name, last_name, first_name FROM customers
JOIN orders on customers.customer_id = orders.customer_id
JOIN employees on orders.employee_id = employees.employee_id
JOIN shippers on orders.ship_via = shippers.shipper_id
WHERE (customers.city, employees.city, shippers.company_name) = ('London', 'London', 'Speedy Express')"""

select_customers_who_did_not_order = """
SELECT customer_id, company_name FROM customers
WHERE customer_id NOT IN
(SELECT DISTINCT customer_id FROM orders)"""

select_products = """
SELECT product_name, units_in_stock, suppliers.contact_name, suppliers.phone FROM products
JOIN suppliers  on products.supplier_id = suppliers.supplier_id
JOIN categories on products.category_id = categories.category_id
WHERE discontinued = 1 AND category_name IN ('Condiments', 'Meat/Poultry')
AND units_in_stock < 100"""

select_units_of_products = """SELECT categories.category_name, sum(units_in_stock) as units FROM products
JOIN categories on categories.category_id = products.category_id
GROUP BY categories.category_name
ORDER BY units DESC"""