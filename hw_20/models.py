from sqlalchemy import (Column, String, SmallInteger, Integer,
                        Float, DateTime, LargeBinary, ForeignKey
                        )
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Customers(Base):
    __tablename__ = 'customers'

    customer_id = Column(String, ForeignKey('orders.customer_id'), primary_key=True)
    company_name = Column(String(40), nullable=False)
    contact_name = Column(String(30))
    contact_title = Column(String(30))
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    phone = Column(String(24))
    fax = Column(String(24))


class Employees(Base):
    __tablename__ = 'employees'

    employee_id = Column(SmallInteger, ForeignKey('orders.employee_id'), primary_key=True)
    last_name = Column(String(20), nullable=False)
    first_name = Column(String(10), nullable=False)
    title = Column(String(30))
    title_of_courtesy = Column(String(25))
    birth_date = Column(DateTime)
    hire_date = Column(DateTime)
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    home_phone = Column(String(24))
    extension = Column(String(4))
    photo = Column(LargeBinary)
    notes = Column(String)
    reports_to = Column(SmallInteger)
    photo_path = Column(String(255))


class Orders(Base):
    __tablename__ = 'orders'

    order_id = Column(SmallInteger, primary_key=True)
    customer_id = Column(String)
    employee_id = Column(SmallInteger)
    order_date = Column(DateTime)
    required_date = Column(DateTime)
    shipped_date = Column(DateTime)
    ship_via = Column(SmallInteger)
    freight = Column(Float)
    ship_name = Column(String(40))
    ship_address = Column(String(60))
    ship_city = Column(String(15))
    ship_region = Column(String(15))
    ship_postal_code = Column(String(10))
    ship_country = Column(String(15))


class Shippers(Base):
    __tablename__ = 'shippers'

    shipper_id = Column(SmallInteger, ForeignKey('orders.ship_via'), primary_key=True)
    company_name = Column(String(40), nullable=False)
    phone = Column(String(24))


class Products(Base):
    __tablename__ = 'products'

    product_id = Column(SmallInteger, primary_key=True)
    product_name = Column(String(40), nullable=False)
    supplier_id = Column(SmallInteger, ForeignKey('suppliers.supplier_id'))
    category_id = Column(SmallInteger, ForeignKey('categories.category_id'))
    quantity_per_unit = Column(String(20))
    unit_price = Column(Float)
    units_in_stock = Column(SmallInteger)
    unit_on_order = Column(SmallInteger)
    recorder_level = Column(SmallInteger)
    discontinued = Column(Integer, nullable=False)


class Suppliers(Base):
    __tablename__ = 'suppliers'

    supplier_id = Column(SmallInteger, primary_key=True)
    company_name = Column(String(40), nullable=False)
    contact_name = Column(String(30))
    address = Column(String(60))
    city = Column(String(15))
    region = Column(String(15))
    postal_code = Column(String(10))
    country = Column(String(15))
    phone = Column(String(24))
    fax = Column(String(24))
    homepage = Column(String)


class Categories(Base):
    __tablename__ = 'categories'

    category_id = Column(SmallInteger, primary_key=True)
    category_name = Column(String(15), nullable=False)
    description = Column(String)
    picture = Column(LargeBinary)

