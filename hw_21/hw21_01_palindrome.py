# Ex. 1
# Написать функцию, которая определяет, является переданная строка палиндромом.
# Написать тесты, в тч параметризационный

import pytest
from typing import Union


def palindrome(line: str) -> Union[bool, None]:
    if type(line) == str:
        if line == line[::-1]:
            return True
        return False
    else:
        print('\nPlease, enter a string\n')


@pytest.mark.parametrize("line, result", [("Kate", False), ("level", True), (12, None)])
def test_palindrome(line, result):
    assert palindrome(line) == result
