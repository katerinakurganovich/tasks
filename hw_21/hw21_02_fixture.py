# Ex.2
# Написать фикстуру, которая открывает соединение к тестовой базе данных, возвращает объект
# сессии/соединения.
# Из less_020 с помощью ORM записать в базу данные о книгах/издателях.
# Написать тест, который проверяет, что все записи были добавлены в бд.
# После выполнения теста соединение к бд должно быть закрыто в фикстуре.

import pytest
import sqlalchemy as db
from hw_20.settings import DB_CONNECTION
from sqlalchemy.orm import sessionmaker
from hw_20.hw20_session import DBSession
from hw21_models import *


@pytest.fixture(scope='module')
def db_connect():
    print('\nConnection\n')
    engine = db.create_engine(DB_CONNECTION)
    Session = sessionmaker(bind=engine)

    yield Session()

    Session().close()
    print('\nClose session\n')


def add_publishers(datas):
    with DBSession(with_commit=True) as session:
        for pub_title, cont in datas:
            publisher = Publisher(title=pub_title, contact=cont)
            session.add(publisher)


def add_book(datas):
    with DBSession(with_commit=True) as session:
        for title, pub_id in datas:
            book = Book(book_title=title, publisher_id=pub_id)
            session.add(book)


books = [('Harry Potter', 1),
         ('War&Peace', 2),
         ('Idiot', 2)]

publishers = [('OREally', '18008898969'),
              ('Ecma', '12345678910'),
              ('Act', '9201820320'),
              ('AcmePub',  '144564899')]


def test_insert_to_db_publishers(db_connect):
    res = db_connect.query(Publisher).all()
    assert len(res) == len(publishers)


def test_insert_to_db_books(db_connect):
    res = db_connect.query(Book).all()
    assert len(res) == len(books)
