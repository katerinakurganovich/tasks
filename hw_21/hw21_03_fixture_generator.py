# Ex.3
# Написать фикстуру, которая генерирует строку случайных символов заданной длины.

import random
import string
import pytest

symbols: str = ''.join((string.ascii_letters, string.digits, string.punctuation))


@pytest.fixture
def generator_str():
    def method(size: int):
        string_of_random_symbols: str = "".join(random.choice(symbols) for _ in range(size))
        return string_of_random_symbols
    return method


def test_fixture(generator_str):
    str_from_generator = generator_str(20)
    assert len(str_from_generator) == 20
    for symbol in set(str_from_generator):
        assert symbol in symbols

