# Ex.4
# Написать функцию, которая подсчитывает число вхождений символов в строке и
# выводит их в порядке убывания.
# Реализацию покрыть параметризационными тестами.
# Дополнительно написать тест, используя фикстуру из Ex.3

import pytest
import string
from typing import List, Union
from hw21_03_fixture_generator import generator_str



def number_of_unique_symbols(line: str) -> Union[List[tuple], str]:
    if type(line) == str:
        set_line = set(line)
        symbols: List[tuple] = [(symbol, line.count(symbol)) for symbol in set_line]
        symbols.sort(key=lambda x: x[1], reverse=True)
        return symbols
    return 'Incorrect type. Please, enter string'


@pytest.mark.parametrize(['line', 'result'], [('aabbb', [('b', 3), ('a', 2)]),
                                              ('aabbbcccc', [('c', 4), ('b', 3), ('a', 2)]),
                                              (12, 'Incorrect type. Please, enter string')])
def test(line, result):
    assert number_of_unique_symbols(line) == result


def test_fixture(generator_str):
    str_from_generator = generator_str(12)
    for symbol in number_of_unique_symbols(str_from_generator):
        assert symbol[0] in ''.join((string.ascii_letters, string.digits, string.punctuation))
