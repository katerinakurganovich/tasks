from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, String, SmallInteger, ForeignKey)

Base = declarative_base()


class Publisher(Base):
    __tablename__ = 'publisher'

    publisher_id = Column(SmallInteger, primary_key=True)
    title = Column(String)
    contact = Column(String)


class Book(Base):
    __tablename__ = 'book'

    book_id = Column(SmallInteger, primary_key=True)
    book_title = Column(String)
    publisher_id = Column(SmallInteger, ForeignKey('publisher.publisher_id'))
