# Ex2
# Дана матрица A[4, 6] и B[?x?]
# Каждая из них содержит экземпляр класса Shape из less16
# Найти результат перемножения 2ух матриц


import numpy
import random
from typing import List
from hw_16.hw_16_04_class_container import Circle, Triangle, Rectangle

shapes: List[object] = [Circle(random.randint(1, 10)),
                        Triangle(random.randint(1, 10), random.randint(1, 10), random.randint(1, 10)),
                        Rectangle(random.randint(1, 10), random.randint(1, 10))]


def mult_matrix(datas):
    first_array = numpy.array([[random.choice(datas) for _ in range(6)]for _ in range(4)])
    second_array = numpy.array([[random.choice(datas) for _ in range(4)]for _ in range(6)])
    return numpy.matmul(first_array, second_array)


def test_matrix():
    matrix_of_shapes = mult_matrix(shapes)
    for row in matrix_of_shapes:
        for elem in row:
            assert elem.__class__.__name__ == 'SomeFigure'

