# Ex.3
# Реализовать класс, который опрашивает ресурс http://numbersapi.com/
# Метод класса должен принимать любое число в соответствии с api и
# сохранять результат в локальном хранилище в формате number: info
# Класс должен иметь возможность преобразовывать информацию в json

import requests
import pytest
from typing import Union
from hw22_session import DBSession
from hw22_models import *


class Response:
    def __init__(self):
        self.resp = None
        self.json_data = None

    def get_info_about_num(self, num: Union[str, int], type_info: str):
        try:
            self.resp = requests.get(f'http://numbersapi.com/{num}/{type_info}', json=True)
            self.json_data = self.resp.json()
            return self.json_data
        except requests.exceptions.RequestException as e:
            print(e.__class__.__name__)

    def get_type_id(self):
        if self.json_data is not None:
            type_name = self.json_data.get('type', None)
            with DBSession() as session:
                types = session.execute("""SELECT * FROM types""")
                data_types = {}
                for type_id, name in types:
                    data_types[name] = type_id
            return data_types[type_name]
        else:
            print('Need to use the run get_info_about_num() first')

    def post_into_db(self):
        if self.json_data is not None:
            req = Request(number=self.json_data.get('number', None),
                          num_info=self.json_data.get('text', None),
                          found=self.json_data.get('found', None),
                          type=self.get_type_id(),
                          date=self.json_data.get('date', None),
                          year=self.json_data.get('year', None))
            with DBSession(with_commit=True) as session:
                session.add(req)
        else:
            print('Need to use the run get_info_about_num() first')


@pytest.mark.parametrize(['num', 'num_type', 'key', 'res'], [(7, 'math', 'number', 7),
                                                             (7, 'math', 'type', 'math'),
                                                             ('1/2', 'date', 'number', 2),
                                                             ('1/2', 'date', 'type', 'date')])
def test_get_info_about_num(num, num_type, key, res):
    resp = Response()
    results = resp.get_info_about_num(num, num_type)
    assert results[key] == res


def test_post_into_db():
    with DBSession() as session:
        count_req_before_post = len(session.query(Request).all())
        resp = Response()
        resp.get_info_about_num(15, 'math')
        resp.post_into_db()
        count_req_after_post = len(session.query(Request).all())
        assert count_req_after_post == count_req_before_post + 1
