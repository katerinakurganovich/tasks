# coding: utf-8
from sqlalchemy import Boolean, Column, ForeignKey, Integer, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Type(Base):
    __tablename__ = 'types'

    id = Column(Integer, primary_key=True, server_default=text("nextval('types_id_seq'::regclass)"))
    name = Column(Text, nullable=False)


class Request(Base):
    __tablename__ = 'requests'

    id = Column(Integer, primary_key=True, server_default=text("nextval('requests_id_seq'::regclass)"))
    number = Column(Integer)
    num_info = Column(Text)
    found = Column(Boolean)
    type = Column(ForeignKey('types.id'))
    date = Column(Text)
    year = Column(Text)

    type1 = relationship('Type')
