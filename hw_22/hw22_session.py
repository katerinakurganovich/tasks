from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DBSession:
    DB_CONNECTION = "postgres://tech:tech@localhost:5432/numbers"
    engine = create_engine(DB_CONNECTION)
    Session = sessionmaker(bind=engine)
    session = Session()

    def __init__(self, with_commit=False):
        self.session = None
        self.commit = with_commit

    def __enter__(self):
        self.session = self.__class__.Session()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.session.rollback()
        else:
            if self.commit:
                self.session.commit()
        self.session.close()


