import socket
from pydantic import BaseModel, validator, ValidationError

client_requests = ['GET C', 'GET F', 'GET DATE', 'GET TIME', 'GET DATETIME', 'POST 100 C', 'POST 100 F', 'GET HIST']


class ClientRequest(BaseModel):
    request: str

    @validator('request')
    @classmethod
    def corr_request(cls, v):
        if v not in client_requests:
            raise ValueError('Неверный запрос')
        return v


def main():
    print('Для выхода из чата наберите: `exit`, `quit` или `q`.')
    HOST = '127.0.0.1'
    PORT = 8008
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        while True:
            mess = input('\nВведите что нибудь >>> ')
            if any(mess.lower() in ext for ext in ['quit', 'exit', 'q']):
                break
            try:
                req = ClientRequest(request=mess)
                mess = req.request.encode('utf-8')
            except ValidationError as e:
                print(e, f'Введите один из следующих запросов {client_requests}')
            else:
                s.sendall(mess)
                data = s.recv(1024)
                print('\nПолучено: ', data.decode('utf-8'))


if __name__ == '__main__':
    main()
