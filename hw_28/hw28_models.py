# coding: utf-8
from sqlalchemy import Column, Integer, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Request(Base):
    __tablename__ = 'requests'

    req_id = Column(Integer, primary_key=True)
    req_datetime = Column(Text)
    req_string = Column(Text)

    def __str__(self):
        return f'{self.req_id} - {self.req_datetime} - {self.req_string}'
