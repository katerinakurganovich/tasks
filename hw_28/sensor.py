import random
from datetime import datetime


class Sensor:
    def __init__(self):
        self.temp = random.randint(-100, 100)

    def curr_temp_c(self):
        temp = self.temp
        return f'TEMP {temp} C'

    def curr_temp_f(self):
        temp = (self.temp * 9/5) + 32
        return f'TEMP {temp} F'

    def set_100_c(self):
        self.temp = 100
        return f'Успешно установлено 100 C'

    def set_100_f(self):
        self.temp = (100 - 32) * 5 / 9
        return f'Успешно установлено 100 F'

    def curr_date(self):
        now = datetime.now()
        return now.strftime("%Y-%m-%d")

    def curr_time(self):
        now = datetime.now()
        return now.strftime("%H:%M:%S")

    def curr_datetime(self):
        now = datetime.now()
        return now.strftime("%Y-%m-%d %H:%M:%S")


