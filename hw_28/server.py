import select
import socket
from sensor import Sensor
from hw28_session import DBSession
from hw28_models import Request

print('Для выключения сервера нажмите Ctrl+C.')
sock = socket.socket()
sock.bind(('localhost', 8008))
sock.listen(5)
sock.setblocking(False)
inst = Sensor()
inputs = [sock]
outputs = []
messages = {}


def logging(req_str):
    with DBSession(with_commit=True) as session:
        curr_datetime = inst.curr_datetime()
        req_inst = Request(req_datetime=curr_datetime, req_string=req_str)
        session.add(req_inst)


def history():
    with DBSession() as session:
        rq = session.query(Request).all()
        rq_str = list(map(lambda x: str(x), rq))
        res = '\n'.join(rq_str)
    return res


client_requests = {'GET C': inst.curr_temp_c,
                   'GET F': inst.curr_temp_f,
                   'GET DATE': inst.curr_date,
                   'GET TIME': inst.curr_time,
                   'GET DATETIME': inst.curr_datetime,
                   'POST 100 C': inst.set_100_c,
                   'POST 100 F': inst.set_100_f,
                   'GET HIST': history
                   }


def main():
    print('\nОжидание подключения...')
    while True:

        reads, send, excepts = select.select(inputs, outputs, inputs)

        for conn in reads:
            if conn == sock:
                new_conn, client_addr = conn.accept()
                print('Успешное подключение!')
                new_conn.setblocking(False)
                inputs.append(new_conn)

            else:

                data = conn.recv(1024)
                if data:
                    if messages.get(conn, None):
                        messages[conn].append(data)
                    else:
                        messages[conn] = [data]
                    if conn not in outputs:
                        outputs.append(conn)
                else:
                    print('Клиент отключился...')
                    if conn in outputs:
                        outputs.remove(conn)
                    inputs.remove(conn)
                    conn.close()
                    del messages[conn]

        for conn in send:
            msg = messages.get(conn, None)
            if len(msg):
                logging(msg[0].decode('utf-8'))
                temp = client_requests[msg.pop(0).decode('utf-8')]()
                conn.send(temp.encode())
            else:
                outputs.remove(conn)

        for conn in excepts:
            print('Клиент отвалился...')
            inputs.remove(conn)
            if conn in outputs:
                outputs.remove(conn)
            conn.close()
            del messages[conn]


if __name__ == '__main__':
    main()

