from rest_framework.serializers import ModelSerializer
from num_app.models import Numbers


class NumberSerializer(ModelSerializer):
    class Meta:
        model = Numbers
        fields = '__all__'
