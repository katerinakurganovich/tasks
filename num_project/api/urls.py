from django.urls import path
from .views import NumberViewSet, CreateNumberView

urlpatterns = [
    path('', NumberViewSet.as_view()),
    path('create/', CreateNumberView.as_view())
]
