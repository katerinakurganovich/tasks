from rest_framework.generics import ListAPIView, CreateAPIView

from .serializers import NumberSerializer
from num_app.models import Numbers


class NumberViewSet(ListAPIView):
    queryset = Numbers.objects.all()
    serializer_class = NumberSerializer


class CreateNumberView(CreateAPIView):
    queryset = Numbers.objects.all()
    serializer_class = NumberSerializer
