from django.db import models


class Numbers(models.Model):
    date = models.DateTimeField(format("%Y-%m-%d%H:%M:%S"))
    request_id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=120, )
    result = models.CharField(max_length=120, )

    def __str__(self):
        return f'{self.date} - {self.request_id} - {self.url} - {self.result}'
