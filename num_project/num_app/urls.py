from django.urls import path
from . import views

urlpatterns = [
    path('', views.random_number),
    path('range/<list_size>', views.random_list),
    path('range/', views.random_list),
    path('history/', views.history)
]
