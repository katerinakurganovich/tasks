import random
from datetime import datetime
from django.http import HttpResponse
from .models import Numbers
from django.shortcuts import render


def random_number(request):
    value = random.randint(1, 100)
    numbers = Numbers(date=datetime.now(), url=request.path, result=value)
    numbers.save()
    return HttpResponse(f"<h4> {value} </h4>")


def random_list(request, list_size=random.randint(1, 20)):
    size = int(list_size)
    list_of_numbers = [random.randint(1, 20) for _ in range(size)]
    numbers = Numbers(date=datetime.now(), url=request.path, result=list_of_numbers)
    numbers.save()
    return HttpResponse(f"<h4> {list_of_numbers} </h4>")


def history(request):
    numbers = Numbers.objects.all()
    lines = [i for i in range(len(numbers)+1) if i % 10 == 0]
    return render(request, 'num_app/history.html', {'numbers': numbers, "lines": lines})
