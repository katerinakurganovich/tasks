from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('numbers/', include('num_app.urls')),
    path('api/', include('api.urls'))
]
